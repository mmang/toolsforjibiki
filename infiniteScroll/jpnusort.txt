 ELSIF tmp = '309B'  THEN result:= result || '02309B'; -- KATAKANA-HIRAGANA VOICED SOUND MARK
 ELSIF tmp = '309C'  THEN result:= result || '02309C'; -- KATAKANA-HIRAGANA SEMI-VOICED SOUND MARK
 ELSIF tmp = '30A0'  THEN result:= result || '0230A0'; -- KATAKANA-HIRAGANA DOUBLE HYPHEN
 ELSIF tmp = '30FB'  THEN result:= result || '0230FB'; -- KATAKANA MIDDLE DOT
 ELSIF tmp = 'FF65'  THEN result:= result || '12FF65'; -- HALFWIDTH KATAKANA MIDDLE DOT
 ELSIF tmp = '30FC'  THEN result:= result || '0230FC'; -- KATAKANA-HIRAGANA PROLONGED SOUND MARK
 ELSIF tmp = 'FF70'  THEN result:= result || '12FF70'; -- HALFWIDTH KATAKANA-HIRAGANA PROLONGED SOUND MARK
 ELSIF tmp = '30FD'  THEN result:= result || '0230FD'; -- KATAKANA ITERATION MARK
 ELSIF tmp = '30FE'  THEN result:= result || '0230FD'; -- KATAKANA VOICED ITERATION MARK
 ELSIF tmp = '30A1'  THEN result:= result || '0F30A1'; -- KATAKANA LETTER SMALL A
 ELSIF tmp = 'FF67'  THEN result:= result || '10FF67'; -- HALFWIDTH KATAKANA LETTER SMALL A
 ELSIF tmp = '30A2'  THEN result:= result || '1130A2'; -- KATAKANA LETTER A
 ELSIF tmp = 'FF71'  THEN result:= result || '12FF71'; -- HALFWIDTH KATAKANA LETTER A
 ELSIF tmp = '32D0'  THEN result:= result || '1332D0'; -- CIRCLED KATAKANA A
 ELSIF tmp = '3043'  THEN result:= result || '0D3043'; -- HIRAGANA LETTER SMALL I
 ELSIF tmp = '3044'  THEN result:= result || '0E3044'; -- HIRAGANA LETTER I
 ELSIF tmp = '30A3'  THEN result:= result || '0F30A3'; -- KATAKANA LETTER SMALL I
 ELSIF tmp = 'FF68'  THEN result:= result || '10FF68'; -- HALFWIDTH KATAKANA LETTER SMALL I
 ELSIF tmp = '30A4'  THEN result:= result || '1130A4'; -- KATAKANA LETTER I
 ELSIF tmp = 'FF72'  THEN result:= result || '12FF72'; -- HALFWIDTH KATAKANA LETTER I
 ELSIF tmp = '32D1'  THEN result:= result || '1332D1'; -- CIRCLED KATAKANA I
 ELSIF tmp = '3041'  THEN result:= result || '0D3041'; -- HIRAGANA LETTER SMALL A
 ELSIF tmp = '3042'  THEN result:= result || '0E3042'; -- HIRAGANA LETTER A
 ELSIF tmp = '30A1'  THEN result:= result || '0F30A1'; -- KATAKANA LETTER SMALL A
 ELSIF tmp = 'FF67'  THEN result:= result || '10FF67'; -- HALFWIDTH KATAKANA LETTER SMALL A
 ELSIF tmp = '30A2'  THEN result:= result || '1130A2'; -- KATAKANA LETTER A
 ELSIF tmp = 'FF71'  THEN result:= result || '12FF71'; -- HALFWIDTH KATAKANA LETTER A
 ELSIF tmp = '32D0'  THEN result:= result || '1332D0'; -- CIRCLED KATAKANA A
 ELSIF tmp = '3303'  THEN result:= result || '1C3303'; -- SQUARE AARU
 ELSIF tmp = '3300'  THEN result:= result || '1C3300'; -- SQUARE APAATO
 ELSIF tmp = '3301'  THEN result:= result || '1C3301'; -- SQUARE ARUHUA
 ELSIF tmp = '3302'  THEN result:= result || '1C3302'; -- SQUARE ANPEA
 ELSIF tmp = '3043'  THEN result:= result || '0D3043'; -- HIRAGANA LETTER SMALL I
 ELSIF tmp = '3044'  THEN result:= result || '0E3044'; -- HIRAGANA LETTER I
 ELSIF tmp = '30A3'  THEN result:= result || '0F30A3'; -- KATAKANA LETTER SMALL I
 ELSIF tmp = 'FF68'  THEN result:= result || '10FF68'; -- HALFWIDTH KATAKANA LETTER SMALL I
 ELSIF tmp = '30A4'  THEN result:= result || '1130A4'; -- KATAKANA LETTER I
 ELSIF tmp = 'FF72'  THEN result:= result || '12FF72'; -- HALFWIDTH KATAKANA LETTER I
 ELSIF tmp = '32D1'  THEN result:= result || '1332D1'; -- CIRCLED KATAKANA I
 ELSIF tmp = '3304'  THEN result:= result || '1C3304'; -- SQUARE ININGU
 ELSIF tmp = '3305'  THEN result:= result || '1C3305'; -- SQUARE INTI
 ELSIF tmp = '3045'  THEN result:= result || '0D3045'; -- HIRAGANA LETTER SMALL U
 ELSIF tmp = '3046'  THEN result:= result || '0E3046'; -- HIRAGANA LETTER U
 ELSIF tmp = '30A5'  THEN result:= result || '0F30A5'; -- KATAKANA LETTER SMALL U
 ELSIF tmp = 'FF69'  THEN result:= result || '10FF69'; -- HALFWIDTH KATAKANA LETTER SMALL U
 ELSIF tmp = '30A6'  THEN result:= result || '1130A6'; -- KATAKANA LETTER U
 ELSIF tmp = 'FF73'  THEN result:= result || '12FF73'; -- HALFWIDTH KATAKANA LETTER U
 ELSIF tmp = '32D2'  THEN result:= result || '1332D2'; -- CIRCLED KATAKANA U
 ELSIF tmp = '3094'  THEN result:= result || '0E3046'; -- HIRAGANA LETTER VU
 ELSIF tmp = '30F4'  THEN result:= result || '1130A6'; -- KATAKANA LETTER VU
 ELSIF tmp = '3306'  THEN result:= result || '1C3306'; -- SQUARE UON
 ELSIF tmp = '1B000'  THEN result:= result || '1130A8'; -- KATAKANA LETTER ARCHAIC E
 ELSIF tmp = '3047'  THEN result:= result || '0D3047'; -- HIRAGANA LETTER SMALL E
 ELSIF tmp = '3048'  THEN result:= result || '0E3048'; -- HIRAGANA LETTER E
 ELSIF tmp = '30A7'  THEN result:= result || '0F30A7'; -- KATAKANA LETTER SMALL E
 ELSIF tmp = 'FF6A'  THEN result:= result || '10FF6A'; -- HALFWIDTH KATAKANA LETTER SMALL E
 ELSIF tmp = '30A8'  THEN result:= result || '1130A8'; -- KATAKANA LETTER E
 ELSIF tmp = 'FF74'  THEN result:= result || '12FF74'; -- HALFWIDTH KATAKANA LETTER E
 ELSIF tmp = '32D3'  THEN result:= result || '1332D3'; -- CIRCLED KATAKANA E
 ELSIF tmp = '3308'  THEN result:= result || '1C3308'; -- SQUARE EEKAA
 ELSIF tmp = '3307'  THEN result:= result || '1C3307'; -- SQUARE ESUKUUDO
 ELSIF tmp = '3049'  THEN result:= result || '0D3049'; -- HIRAGANA LETTER SMALL O
 ELSIF tmp = '304A'  THEN result:= result || '0E304A'; -- HIRAGANA LETTER O
 ELSIF tmp = '30A9'  THEN result:= result || '0F30A9'; -- KATAKANA LETTER SMALL O
 ELSIF tmp = 'FF6B'  THEN result:= result || '10FF6B'; -- HALFWIDTH KATAKANA LETTER SMALL O
 ELSIF tmp = '30AA'  THEN result:= result || '1130AA'; -- KATAKANA LETTER O
 ELSIF tmp = 'FF75'  THEN result:= result || '12FF75'; -- HALFWIDTH KATAKANA LETTER O
 ELSIF tmp = '32D4'  THEN result:= result || '1332D4'; -- CIRCLED KATAKANA O
 ELSIF tmp = '330A'  THEN result:= result || '1C330A'; -- SQUARE OOMU
 ELSIF tmp = '3309'  THEN result:= result || '1C3309'; -- SQUARE ONSU
 ELSIF tmp = '3095'  THEN result:= result || '0D3095'; -- HIRAGANA LETTER SMALL KA
 ELSIF tmp = '304B'  THEN result:= result || '0E304B'; -- HIRAGANA LETTER KA
 ELSIF tmp = '30F5'  THEN result:= result || '0F30F5'; -- KATAKANA LETTER SMALL KA
 ELSIF tmp = '30AB'  THEN result:= result || '1130AB'; -- KATAKANA LETTER KA
 ELSIF tmp = 'FF76'  THEN result:= result || '12FF76'; -- HALFWIDTH KATAKANA LETTER KA
 ELSIF tmp = '32D5'  THEN result:= result || '1332D5'; -- CIRCLED KATAKANA KA
 ELSIF tmp = '304C'  THEN result:= result || '0E304B'; -- HIRAGANA LETTER GA
 ELSIF tmp = '30AC'  THEN result:= result || '1130AB'; -- KATAKANA LETTER GA
 ELSIF tmp = '330B'  THEN result:= result || '1C330B'; -- SQUARE KAIRI
 ELSIF tmp = '330C'  THEN result:= result || '1C330C'; -- SQUARE KARATTO
 ELSIF tmp = '330D'  THEN result:= result || '1C330D'; -- SQUARE KARORII
 ELSIF tmp = '330E'  THEN result:= result || '1C330E'; -- SQUARE GARON
 ELSIF tmp = '330F'  THEN result:= result || '1C330F'; -- SQUARE GANMA
 ELSIF tmp = '304D'  THEN result:= result || '0E304D'; -- HIRAGANA LETTER KI
 ELSIF tmp = '30AD'  THEN result:= result || '1130AD'; -- KATAKANA LETTER KI
 ELSIF tmp = 'FF77'  THEN result:= result || '12FF77'; -- HALFWIDTH KATAKANA LETTER KI
 ELSIF tmp = '32D6'  THEN result:= result || '1332D6'; -- CIRCLED KATAKANA KI
 ELSIF tmp = '304E'  THEN result:= result || '0E304D'; -- HIRAGANA LETTER GI
 ELSIF tmp = '30AE'  THEN result:= result || '1130AD'; -- KATAKANA LETTER GI
 ELSIF tmp = '3310'  THEN result:= result || '1C3310'; -- SQUARE GIGA
 ELSIF tmp = '3311'  THEN result:= result || '1C3311'; -- SQUARE GINII
 ELSIF tmp = '3312'  THEN result:= result || '1C3312'; -- SQUARE KYURII
 ELSIF tmp = '3313'  THEN result:= result || '1C3313'; -- SQUARE GIRUDAA
 ELSIF tmp = '3314'  THEN result:= result || '1C3314'; -- SQUARE KIRO
 ELSIF tmp = '3315'  THEN result:= result || '1C3315'; -- SQUARE KIROGURAMU
 ELSIF tmp = '3316'  THEN result:= result || '1C3316'; -- SQUARE KIROMEETORU
 ELSIF tmp = '3317'  THEN result:= result || '1C3317'; -- SQUARE KIROWATTO
 ELSIF tmp = '304F'  THEN result:= result || '0E304F'; -- HIRAGANA LETTER KU
 ELSIF tmp = '31F0'  THEN result:= result || '0F31F0'; -- KATAKANA LETTER SMALL KU
 ELSIF tmp = '30AF'  THEN result:= result || '1130AF'; -- KATAKANA LETTER KU
 ELSIF tmp = 'FF78'  THEN result:= result || '12FF78'; -- HALFWIDTH KATAKANA LETTER KU
 ELSIF tmp = '32D7'  THEN result:= result || '1332D7'; -- CIRCLED KATAKANA KU
 ELSIF tmp = '3050'  THEN result:= result || '0E304F'; -- HIRAGANA LETTER GU
 ELSIF tmp = '30B0'  THEN result:= result || '1130AF'; -- KATAKANA LETTER GU
 ELSIF tmp = '3318'  THEN result:= result || '1C3318'; -- SQUARE GURAMU
 ELSIF tmp = '3319'  THEN result:= result || '1C3319'; -- SQUARE GURAMUTON
 ELSIF tmp = '331A'  THEN result:= result || '1C331A'; -- SQUARE KURUZEIRO
 ELSIF tmp = '331B'  THEN result:= result || '1C331B'; -- SQUARE KUROONE
 ELSIF tmp = '3096'  THEN result:= result || '0D3096'; -- HIRAGANA LETTER SMALL KE
 ELSIF tmp = '3051'  THEN result:= result || '0E3051'; -- HIRAGANA LETTER KE
 ELSIF tmp = '30F6'  THEN result:= result || '0F30F6'; -- KATAKANA LETTER SMALL KE
 ELSIF tmp = '30B1'  THEN result:= result || '1130B1'; -- KATAKANA LETTER KE
 ELSIF tmp = 'FF79'  THEN result:= result || '12FF79'; -- HALFWIDTH KATAKANA LETTER KE
 ELSIF tmp = '32D8'  THEN result:= result || '1332D8'; -- CIRCLED KATAKANA KE
 ELSIF tmp = '3052'  THEN result:= result || '0E3051'; -- HIRAGANA LETTER GE
 ELSIF tmp = '30B2'  THEN result:= result || '1130B1'; -- KATAKANA LETTER GE
 ELSIF tmp = '331C'  THEN result:= result || '1C331C'; -- SQUARE KEESU
 ELSIF tmp = '3053'  THEN result:= result || '0E3053'; -- HIRAGANA LETTER KO
 ELSIF tmp = '30B3'  THEN result:= result || '1130B3'; -- KATAKANA LETTER KO
 ELSIF tmp = 'FF7A'  THEN result:= result || '12FF7A'; -- HALFWIDTH KATAKANA LETTER KO
 ELSIF tmp = '32D9'  THEN result:= result || '1332D9'; -- CIRCLED KATAKANA KO
 ELSIF tmp = '3054'  THEN result:= result || '0E3053'; -- HIRAGANA LETTER GO
 ELSIF tmp = '30B4'  THEN result:= result || '1130B3'; -- KATAKANA LETTER GO
 ELSIF tmp = '331E'  THEN result:= result || '1C331E'; -- SQUARE KOOPO
 ELSIF tmp = '30FF'  THEN result:= result || '1630FF'; -- KATAKANA DIGRAPH KOTO
 ELSIF tmp = '331D'  THEN result:= result || '1C331D'; -- SQUARE KORUNA
 ELSIF tmp = '3055'  THEN result:= result || '0E3055'; -- HIRAGANA LETTER SA
 ELSIF tmp = '30B5'  THEN result:= result || '1130B5'; -- KATAKANA LETTER SA
 ELSIF tmp = 'FF7B'  THEN result:= result || '12FF7B'; -- HALFWIDTH KATAKANA LETTER SA
 ELSIF tmp = '32DA'  THEN result:= result || '1332DA'; -- CIRCLED KATAKANA SA
 ELSIF tmp = '3056'  THEN result:= result || '0E3055'; -- HIRAGANA LETTER ZA
 ELSIF tmp = '30B6'  THEN result:= result || '1130B5'; -- KATAKANA LETTER ZA
 ELSIF tmp = '331F'  THEN result:= result || '1C331F'; -- SQUARE SAIKURU
 ELSIF tmp = '3320'  THEN result:= result || '1C3320'; -- SQUARE SANTIIMU
 ELSIF tmp = '3057'  THEN result:= result || '0E3057'; -- HIRAGANA LETTER SI
 ELSIF tmp = '31F1'  THEN result:= result || '0F31F1'; -- KATAKANA LETTER SMALL SI
 ELSIF tmp = '30B7'  THEN result:= result || '1130B7'; -- KATAKANA LETTER SI
 ELSIF tmp = 'FF7C'  THEN result:= result || '12FF7C'; -- HALFWIDTH KATAKANA LETTER SI
 ELSIF tmp = '32DB'  THEN result:= result || '1332DB'; -- CIRCLED KATAKANA SI
 ELSIF tmp = '3058'  THEN result:= result || '0E3057'; -- HIRAGANA LETTER ZI
 ELSIF tmp = '30B8'  THEN result:= result || '1130B7'; -- KATAKANA LETTER ZI
 ELSIF tmp = '3006'  THEN result:= result || '043006'; -- IDEOGRAPHIC CLOSING MARK
 ELSIF tmp = '3321'  THEN result:= result || '1C3321'; -- SQUARE SIRINGU
 ELSIF tmp = '3059'  THEN result:= result || '0E3059'; -- HIRAGANA LETTER SU
 ELSIF tmp = '31F2'  THEN result:= result || '0F31F2'; -- KATAKANA LETTER SMALL SU
 ELSIF tmp = '30B9'  THEN result:= result || '1130B9'; -- KATAKANA LETTER SU
 ELSIF tmp = 'FF7D'  THEN result:= result || '12FF7D'; -- HALFWIDTH KATAKANA LETTER SU
 ELSIF tmp = '32DC'  THEN result:= result || '1332DC'; -- CIRCLED KATAKANA SU
 ELSIF tmp = '305A'  THEN result:= result || '0E3059'; -- HIRAGANA LETTER ZU
 ELSIF tmp = '30BA'  THEN result:= result || '1130B9'; -- KATAKANA LETTER ZU
 ELSIF tmp = '305B'  THEN result:= result || '0E305B'; -- HIRAGANA LETTER SE
 ELSIF tmp = '30BB'  THEN result:= result || '1130BB'; -- KATAKANA LETTER SE
 ELSIF tmp = 'FF7E'  THEN result:= result || '12FF7E'; -- HALFWIDTH KATAKANA LETTER SE
 ELSIF tmp = '32DD'  THEN result:= result || '1332DD'; -- CIRCLED KATAKANA SE
 ELSIF tmp = '305C'  THEN result:= result || '0E305B'; -- HIRAGANA LETTER ZE
 ELSIF tmp = '30BC'  THEN result:= result || '1130BB'; -- KATAKANA LETTER ZE
 ELSIF tmp = '3322'  THEN result:= result || '1C3322'; -- SQUARE SENTI
 ELSIF tmp = '3323'  THEN result:= result || '1C3323'; -- SQUARE SENTO
 ELSIF tmp = '305D'  THEN result:= result || '0E305D'; -- HIRAGANA LETTER SO
 ELSIF tmp = '30BD'  THEN result:= result || '1130BD'; -- KATAKANA LETTER SO
 ELSIF tmp = 'FF7F'  THEN result:= result || '12FF7F'; -- HALFWIDTH KATAKANA LETTER SO
 ELSIF tmp = '32DE'  THEN result:= result || '1332DE'; -- CIRCLED KATAKANA SO
 ELSIF tmp = '305E'  THEN result:= result || '0E305D'; -- HIRAGANA LETTER ZO
 ELSIF tmp = '30BE'  THEN result:= result || '1130BD'; -- KATAKANA LETTER ZO
 ELSIF tmp = '305F'  THEN result:= result || '0E305F'; -- HIRAGANA LETTER TA
 ELSIF tmp = '30BF'  THEN result:= result || '1130BF'; -- KATAKANA LETTER TA
 ELSIF tmp = 'FF80'  THEN result:= result || '12FF80'; -- HALFWIDTH KATAKANA LETTER TA
 ELSIF tmp = '32DF'  THEN result:= result || '1332DF'; -- CIRCLED KATAKANA TA
 ELSIF tmp = '3060'  THEN result:= result || '0E305F'; -- HIRAGANA LETTER DA
 ELSIF tmp = '30C0'  THEN result:= result || '1130BF'; -- KATAKANA LETTER DA
 ELSIF tmp = '3324'  THEN result:= result || '1C3324'; -- SQUARE DAASU
 ELSIF tmp = '3061'  THEN result:= result || '0E3061'; -- HIRAGANA LETTER TI
 ELSIF tmp = '30C1'  THEN result:= result || '1130C1'; -- KATAKANA LETTER TI
 ELSIF tmp = 'FF81'  THEN result:= result || '12FF81'; -- HALFWIDTH KATAKANA LETTER TI
 ELSIF tmp = '32E0'  THEN result:= result || '1332E0'; -- CIRCLED KATAKANA TI
 ELSIF tmp = '3062'  THEN result:= result || '0E3061'; -- HIRAGANA LETTER DI
 ELSIF tmp = '30C2'  THEN result:= result || '1130C1'; -- KATAKANA LETTER DI
 ELSIF tmp = '3063'  THEN result:= result || '0D3063'; -- HIRAGANA LETTER SMALL TU
 ELSIF tmp = '3064'  THEN result:= result || '0E3064'; -- HIRAGANA LETTER TU
 ELSIF tmp = '30C3'  THEN result:= result || '0F30C3'; -- KATAKANA LETTER SMALL TU
 ELSIF tmp = 'FF6F'  THEN result:= result || '10FF6F'; -- HALFWIDTH KATAKANA LETTER SMALL TU
 ELSIF tmp = '30C4'  THEN result:= result || '1130C4'; -- KATAKANA LETTER TU
 ELSIF tmp = 'FF82'  THEN result:= result || '12FF82'; -- HALFWIDTH KATAKANA LETTER TU
 ELSIF tmp = '32E1'  THEN result:= result || '1332E1'; -- CIRCLED KATAKANA TU
 ELSIF tmp = '3065'  THEN result:= result || '0E3064'; -- HIRAGANA LETTER DU
 ELSIF tmp = '30C5'  THEN result:= result || '1130C4'; -- KATAKANA LETTER DU
 ELSIF tmp = '3066'  THEN result:= result || '0E3066'; -- HIRAGANA LETTER TE
 ELSIF tmp = '30C6'  THEN result:= result || '1130C6'; -- KATAKANA LETTER TE
 ELSIF tmp = 'FF83'  THEN result:= result || '12FF83'; -- HALFWIDTH KATAKANA LETTER TE
 ELSIF tmp = '32E2'  THEN result:= result || '1332E2'; -- CIRCLED KATAKANA TE
 ELSIF tmp = '3067'  THEN result:= result || '0E3066'; -- HIRAGANA LETTER DE
 ELSIF tmp = '30C7'  THEN result:= result || '1130C6'; -- KATAKANA LETTER DE
 ELSIF tmp = '3325'  THEN result:= result || '1C3325'; -- SQUARE DESI
 ELSIF tmp = '3068'  THEN result:= result || '0E3068'; -- HIRAGANA LETTER TO
 ELSIF tmp = '31F3'  THEN result:= result || '0F31F3'; -- KATAKANA LETTER SMALL TO
 ELSIF tmp = '30C8'  THEN result:= result || '1130C8'; -- KATAKANA LETTER TO
 ELSIF tmp = 'FF84'  THEN result:= result || '12FF84'; -- HALFWIDTH KATAKANA LETTER TO
 ELSIF tmp = '32E3'  THEN result:= result || '1332E3'; -- CIRCLED KATAKANA TO
 ELSIF tmp = '3069'  THEN result:= result || '0E3068'; -- HIRAGANA LETTER DO
 ELSIF tmp = '30C9'  THEN result:= result || '1130C8'; -- KATAKANA LETTER DO
 ELSIF tmp = '3326'  THEN result:= result || '1C3326'; -- SQUARE DORU
 ELSIF tmp = '3327'  THEN result:= result || '1C3327'; -- SQUARE TON
 ELSIF tmp = '306A'  THEN result:= result || '0E306A'; -- HIRAGANA LETTER NA
 ELSIF tmp = '30CA'  THEN result:= result || '1130CA'; -- KATAKANA LETTER NA
 ELSIF tmp = 'FF85'  THEN result:= result || '12FF85'; -- HALFWIDTH KATAKANA LETTER NA
 ELSIF tmp = '32E4'  THEN result:= result || '1332E4'; -- CIRCLED KATAKANA NA
 ELSIF tmp = '3328'  THEN result:= result || '1C3328'; -- SQUARE NANO
 ELSIF tmp = '306B'  THEN result:= result || '0E306B'; -- HIRAGANA LETTER NI
 ELSIF tmp = '30CB'  THEN result:= result || '1130CB'; -- KATAKANA LETTER NI
 ELSIF tmp = 'FF86'  THEN result:= result || '12FF86'; -- HALFWIDTH KATAKANA LETTER NI
 ELSIF tmp = '32E5'  THEN result:= result || '1332E5'; -- CIRCLED KATAKANA NI
 ELSIF tmp = '306C'  THEN result:= result || '0E306C'; -- HIRAGANA LETTER NU
 ELSIF tmp = '31F4'  THEN result:= result || '0F31F4'; -- KATAKANA LETTER SMALL NU
 ELSIF tmp = '30CC'  THEN result:= result || '1130CC'; -- KATAKANA LETTER NU
 ELSIF tmp = 'FF87'  THEN result:= result || '12FF87'; -- HALFWIDTH KATAKANA LETTER NU
 ELSIF tmp = '32E6'  THEN result:= result || '1332E6'; -- CIRCLED KATAKANA NU
 ELSIF tmp = '306D'  THEN result:= result || '0E306D'; -- HIRAGANA LETTER NE
 ELSIF tmp = '30CD'  THEN result:= result || '1130CD'; -- KATAKANA LETTER NE
 ELSIF tmp = 'FF88'  THEN result:= result || '12FF88'; -- HALFWIDTH KATAKANA LETTER NE
 ELSIF tmp = '32E7'  THEN result:= result || '1332E7'; -- CIRCLED KATAKANA NE
 ELSIF tmp = '306E'  THEN result:= result || '0E306E'; -- HIRAGANA LETTER NO
 ELSIF tmp = '30CE'  THEN result:= result || '1130CE'; -- KATAKANA LETTER NO
 ELSIF tmp = 'FF89'  THEN result:= result || '12FF89'; -- HALFWIDTH KATAKANA LETTER NO
 ELSIF tmp = '32E8'  THEN result:= result || '1332E8'; -- CIRCLED KATAKANA NO
 ELSIF tmp = '3329'  THEN result:= result || '1C3329'; -- SQUARE NOTTO
 ELSIF tmp = '306F'  THEN result:= result || '0E306F'; -- HIRAGANA LETTER HA
 ELSIF tmp = '31F5'  THEN result:= result || '0F31F5'; -- KATAKANA LETTER SMALL HA
 ELSIF tmp = '30CF'  THEN result:= result || '1130CF'; -- KATAKANA LETTER HA
 ELSIF tmp = 'FF8A'  THEN result:= result || '12FF8A'; -- HALFWIDTH KATAKANA LETTER HA
 ELSIF tmp = '32E9'  THEN result:= result || '1332E9'; -- CIRCLED KATAKANA HA
 ELSIF tmp = '3070'  THEN result:= result || '0E306F'; -- HIRAGANA LETTER BA
 ELSIF tmp = '30D0'  THEN result:= result || '1130CF'; -- KATAKANA LETTER BA
 ELSIF tmp = '3071'  THEN result:= result || '0E306F'; -- HIRAGANA LETTER PA
 ELSIF tmp = '30D1'  THEN result:= result || '1130CF'; -- KATAKANA LETTER PA
 ELSIF tmp = '332B'  THEN result:= result || '1C332B'; -- SQUARE PAASENTO
 ELSIF tmp = '332C'  THEN result:= result || '1C332C'; -- SQUARE PAATU
 ELSIF tmp = '332D'  THEN result:= result || '1C332D'; -- SQUARE BAARERU
 ELSIF tmp = '332A'  THEN result:= result || '1C332A'; -- SQUARE HAITU
 ELSIF tmp = '3072'  THEN result:= result || '0E3072'; -- HIRAGANA LETTER HI
 ELSIF tmp = '31F6'  THEN result:= result || '0F31F6'; -- KATAKANA LETTER SMALL HI
 ELSIF tmp = '30D2'  THEN result:= result || '1130D2'; -- KATAKANA LETTER HI
 ELSIF tmp = 'FF8B'  THEN result:= result || '12FF8B'; -- HALFWIDTH KATAKANA LETTER HI
 ELSIF tmp = '32EA'  THEN result:= result || '1332EA'; -- CIRCLED KATAKANA HI
 ELSIF tmp = '3073'  THEN result:= result || '0E3072'; -- HIRAGANA LETTER BI
 ELSIF tmp = '30D3'  THEN result:= result || '1130D2'; -- KATAKANA LETTER BI
 ELSIF tmp = '3074'  THEN result:= result || '0E3072'; -- HIRAGANA LETTER PI
 ELSIF tmp = '30D4'  THEN result:= result || '1130D2'; -- KATAKANA LETTER PI
 ELSIF tmp = '332E'  THEN result:= result || '1C332E'; -- SQUARE PIASUTORU
 ELSIF tmp = '332F'  THEN result:= result || '1C332F'; -- SQUARE PIKURU
 ELSIF tmp = '3330'  THEN result:= result || '1C3330'; -- SQUARE PIKO
 ELSIF tmp = '3331'  THEN result:= result || '1C3331'; -- SQUARE BIRU
 ELSIF tmp = '3075'  THEN result:= result || '0E3075'; -- HIRAGANA LETTER HU
 ELSIF tmp = '31F7'  THEN result:= result || '0F31F7'; -- KATAKANA LETTER SMALL HU
 ELSIF tmp = '30D5'  THEN result:= result || '1130D5'; -- KATAKANA LETTER HU
 ELSIF tmp = 'FF8C'  THEN result:= result || '12FF8C'; -- HALFWIDTH KATAKANA LETTER HU
 ELSIF tmp = '32EB'  THEN result:= result || '1332EB'; -- CIRCLED KATAKANA HU
 ELSIF tmp = '3076'  THEN result:= result || '0E3075'; -- HIRAGANA LETTER BU
 ELSIF tmp = '30D6'  THEN result:= result || '1130D5'; -- KATAKANA LETTER BU
 ELSIF tmp = '3077'  THEN result:= result || '0E3075'; -- HIRAGANA LETTER PU
 ELSIF tmp = '30D7'  THEN result:= result || '1130D5'; -- KATAKANA LETTER PU
 ELSIF tmp = '3332'  THEN result:= result || '1C3332'; -- SQUARE HUARADDO
 ELSIF tmp = '3333'  THEN result:= result || '1C3333'; -- SQUARE HUIITO
 ELSIF tmp = '3334'  THEN result:= result || '1C3334'; -- SQUARE BUSSYERU
 ELSIF tmp = '3335'  THEN result:= result || '1C3335'; -- SQUARE HURAN
 ELSIF tmp = '3078'  THEN result:= result || '0E3078'; -- HIRAGANA LETTER HE
 ELSIF tmp = '31F8'  THEN result:= result || '0F31F8'; -- KATAKANA LETTER SMALL HE
 ELSIF tmp = '30D8'  THEN result:= result || '1130D8'; -- KATAKANA LETTER HE
 ELSIF tmp = 'FF8D'  THEN result:= result || '12FF8D'; -- HALFWIDTH KATAKANA LETTER HE
 ELSIF tmp = '32EC'  THEN result:= result || '1332EC'; -- CIRCLED KATAKANA HE
 ELSIF tmp = '3079'  THEN result:= result || '0E3078'; -- HIRAGANA LETTER BE
 ELSIF tmp = '30D9'  THEN result:= result || '1130D8'; -- KATAKANA LETTER BE
 ELSIF tmp = '307A'  THEN result:= result || '0E3078'; -- HIRAGANA LETTER PE
 ELSIF tmp = '30DA'  THEN result:= result || '1130D8'; -- KATAKANA LETTER PE
 ELSIF tmp = '333B'  THEN result:= result || '1C333B'; -- SQUARE PEEZI
 ELSIF tmp = '333C'  THEN result:= result || '1C333C'; -- SQUARE BEETA
 ELSIF tmp = '3336'  THEN result:= result || '1C3336'; -- SQUARE HEKUTAARU
 ELSIF tmp = '3337'  THEN result:= result || '1C3337'; -- SQUARE PESO
 ELSIF tmp = '3338'  THEN result:= result || '1C3338'; -- SQUARE PENIHI
 ELSIF tmp = '3339'  THEN result:= result || '1C3339'; -- SQUARE HERUTU
 ELSIF tmp = '333A'  THEN result:= result || '1C333A'; -- SQUARE PENSU
 ELSIF tmp = '307B'  THEN result:= result || '0E307B'; -- HIRAGANA LETTER HO
 ELSIF tmp = '31F9'  THEN result:= result || '0F31F9'; -- KATAKANA LETTER SMALL HO
 ELSIF tmp = '30DB'  THEN result:= result || '1130DB'; -- KATAKANA LETTER HO
 ELSIF tmp = 'FF8E'  THEN result:= result || '12FF8E'; -- HALFWIDTH KATAKANA LETTER HO
 ELSIF tmp = '32ED'  THEN result:= result || '1332ED'; -- CIRCLED KATAKANA HO
 ELSIF tmp = '307C'  THEN result:= result || '0E307B'; -- HIRAGANA LETTER BO
 ELSIF tmp = '30DC'  THEN result:= result || '1130DB'; -- KATAKANA LETTER BO
 ELSIF tmp = '307D'  THEN result:= result || '0E307B'; -- HIRAGANA LETTER PO
 ELSIF tmp = '30DD'  THEN result:= result || '1130DB'; -- KATAKANA LETTER PO
 ELSIF tmp = '3341'  THEN result:= result || '1C3341'; -- SQUARE HOORU
 ELSIF tmp = '3342'  THEN result:= result || '1C3342'; -- SQUARE HOON
 ELSIF tmp = '333D'  THEN result:= result || '1C333D'; -- SQUARE POINTO
 ELSIF tmp = '333E'  THEN result:= result || '1C333E'; -- SQUARE BORUTO
 ELSIF tmp = '333F'  THEN result:= result || '1C333F'; -- SQUARE HON
 ELSIF tmp = '3340'  THEN result:= result || '1C3340'; -- SQUARE PONDO
 ELSIF tmp = '307E'  THEN result:= result || '0E307E'; -- HIRAGANA LETTER MA
 ELSIF tmp = '30DE'  THEN result:= result || '1130DE'; -- KATAKANA LETTER MA
 ELSIF tmp = 'FF8F'  THEN result:= result || '12FF8F'; -- HALFWIDTH KATAKANA LETTER MA
 ELSIF tmp = '32EE'  THEN result:= result || '1332EE'; -- CIRCLED KATAKANA MA
 ELSIF tmp = '3343'  THEN result:= result || '1C3343'; -- SQUARE MAIKURO
 ELSIF tmp = '3344'  THEN result:= result || '1C3344'; -- SQUARE MAIRU
 ELSIF tmp = '303C'  THEN result:= result || '04303C'; -- MASU MARK
 ELSIF tmp = '3345'  THEN result:= result || '1C3345'; -- SQUARE MAHHA
 ELSIF tmp = '3346'  THEN result:= result || '1C3346'; -- SQUARE MARUKU
 ELSIF tmp = '3347'  THEN result:= result || '1C3347'; -- SQUARE MANSYON
 ELSIF tmp = '307F'  THEN result:= result || '0E307F'; -- HIRAGANA LETTER MI
 ELSIF tmp = '30DF'  THEN result:= result || '1130DF'; -- KATAKANA LETTER MI
 ELSIF tmp = 'FF90'  THEN result:= result || '12FF90'; -- HALFWIDTH KATAKANA LETTER MI
 ELSIF tmp = '32EF'  THEN result:= result || '1332EF'; -- CIRCLED KATAKANA MI
 ELSIF tmp = '3348'  THEN result:= result || '1C3348'; -- SQUARE MIKURON
 ELSIF tmp = '3349'  THEN result:= result || '1C3349'; -- SQUARE MIRI
 ELSIF tmp = '334A'  THEN result:= result || '1C334A'; -- SQUARE MIRIBAARU
 ELSIF tmp = '3080'  THEN result:= result || '0E3080'; -- HIRAGANA LETTER MU
 ELSIF tmp = '31FA'  THEN result:= result || '0F31FA'; -- KATAKANA LETTER SMALL MU
 ELSIF tmp = '30E0'  THEN result:= result || '1130E0'; -- KATAKANA LETTER MU
 ELSIF tmp = 'FF91'  THEN result:= result || '12FF91'; -- HALFWIDTH KATAKANA LETTER MU
 ELSIF tmp = '32F0'  THEN result:= result || '1332F0'; -- CIRCLED KATAKANA MU
 ELSIF tmp = '3081'  THEN result:= result || '0E3081'; -- HIRAGANA LETTER ME
 ELSIF tmp = '30E1'  THEN result:= result || '1130E1'; -- KATAKANA LETTER ME
 ELSIF tmp = 'FF92'  THEN result:= result || '12FF92'; -- HALFWIDTH KATAKANA LETTER ME
 ELSIF tmp = '32F1'  THEN result:= result || '1332F1'; -- CIRCLED KATAKANA ME
 ELSIF tmp = '334D'  THEN result:= result || '1C334D'; -- SQUARE MEETORU
 ELSIF tmp = '334B'  THEN result:= result || '1C334B'; -- SQUARE MEGA
 ELSIF tmp = '334C'  THEN result:= result || '1C334C'; -- SQUARE MEGATON
 ELSIF tmp = '3082'  THEN result:= result || '0E3082'; -- HIRAGANA LETTER MO
 ELSIF tmp = '30E2'  THEN result:= result || '1130E2'; -- KATAKANA LETTER MO
 ELSIF tmp = 'FF93'  THEN result:= result || '12FF93'; -- HALFWIDTH KATAKANA LETTER MO
 ELSIF tmp = '32F2'  THEN result:= result || '1332F2'; -- CIRCLED KATAKANA MO
 ELSIF tmp = '3083'  THEN result:= result || '0D3083'; -- HIRAGANA LETTER SMALL YA
 ELSIF tmp = '3084'  THEN result:= result || '0E3084'; -- HIRAGANA LETTER YA
 ELSIF tmp = '30E3'  THEN result:= result || '0F30E3'; -- KATAKANA LETTER SMALL YA
 ELSIF tmp = 'FF6C'  THEN result:= result || '10FF6C'; -- HALFWIDTH KATAKANA LETTER SMALL YA
 ELSIF tmp = '30E4'  THEN result:= result || '1130E4'; -- KATAKANA LETTER YA
 ELSIF tmp = 'FF94'  THEN result:= result || '12FF94'; -- HALFWIDTH KATAKANA LETTER YA
 ELSIF tmp = '32F3'  THEN result:= result || '1332F3'; -- CIRCLED KATAKANA YA
 ELSIF tmp = '334E'  THEN result:= result || '1C334E'; -- SQUARE YAADO
 ELSIF tmp = '334F'  THEN result:= result || '1C334F'; -- SQUARE YAARU
 ELSIF tmp = '3085'  THEN result:= result || '0D3085'; -- HIRAGANA LETTER SMALL YU
 ELSIF tmp = '3086'  THEN result:= result || '0E3086'; -- HIRAGANA LETTER YU
 ELSIF tmp = '30E5'  THEN result:= result || '0F30E5'; -- KATAKANA LETTER SMALL YU
 ELSIF tmp = 'FF6D'  THEN result:= result || '10FF6D'; -- HALFWIDTH KATAKANA LETTER SMALL YU
 ELSIF tmp = '30E6'  THEN result:= result || '1130E6'; -- KATAKANA LETTER YU
 ELSIF tmp = 'FF95'  THEN result:= result || '12FF95'; -- HALFWIDTH KATAKANA LETTER YU
 ELSIF tmp = '32F4'  THEN result:= result || '1332F4'; -- CIRCLED KATAKANA YU
 ELSIF tmp = '3350'  THEN result:= result || '1C3350'; -- SQUARE YUAN
 ELSIF tmp = '1B001'  THEN result:= result || '0E3086'; -- SQUARE YUAN
 ELSIF tmp = '3087'  THEN result:= result || '0D3087'; -- HIRAGANA LETTER SMALL YO
 ELSIF tmp = '3088'  THEN result:= result || '0E3088'; -- HIRAGANA LETTER YO
 ELSIF tmp = '30E7'  THEN result:= result || '0F30E7'; -- KATAKANA LETTER SMALL YO
 ELSIF tmp = 'FF6E'  THEN result:= result || '10FF6E'; -- HALFWIDTH KATAKANA LETTER SMALL YO
 ELSIF tmp = '30E8'  THEN result:= result || '1130E8'; -- KATAKANA LETTER YO
 ELSIF tmp = 'FF96'  THEN result:= result || '12FF96'; -- HALFWIDTH KATAKANA LETTER YO
 ELSIF tmp = '32F5'  THEN result:= result || '1332F5'; -- CIRCLED KATAKANA YO
 ELSIF tmp = '309F'  THEN result:= result || '16309F'; -- HIRAGANA DIGRAPH YORI
 ELSIF tmp = '3089'  THEN result:= result || '0E3089'; -- HIRAGANA LETTER RA
 ELSIF tmp = '31FB'  THEN result:= result || '0F31FB'; -- KATAKANA LETTER SMALL RA
 ELSIF tmp = '30E9'  THEN result:= result || '1130E9'; -- KATAKANA LETTER RA
 ELSIF tmp = 'FF97'  THEN result:= result || '12FF97'; -- HALFWIDTH KATAKANA LETTER RA
 ELSIF tmp = '32F6'  THEN result:= result || '1332F6'; -- CIRCLED KATAKANA RA
 ELSIF tmp = '308A'  THEN result:= result || '0E308A'; -- HIRAGANA LETTER RI
 ELSIF tmp = '31FC'  THEN result:= result || '0F31FC'; -- KATAKANA LETTER SMALL RI
 ELSIF tmp = '30EA'  THEN result:= result || '1130EA'; -- KATAKANA LETTER RI
 ELSIF tmp = 'FF98'  THEN result:= result || '12FF98'; -- HALFWIDTH KATAKANA LETTER RI
 ELSIF tmp = '32F7'  THEN result:= result || '1332F7'; -- CIRCLED KATAKANA RI
 ELSIF tmp = '3351'  THEN result:= result || '1C3351'; -- SQUARE RITTORU
 ELSIF tmp = '3352'  THEN result:= result || '1C3352'; -- SQUARE RIRA
 ELSIF tmp = '308B'  THEN result:= result || '0E308B'; -- HIRAGANA LETTER RU
 ELSIF tmp = '31FD'  THEN result:= result || '0F31FD'; -- KATAKANA LETTER SMALL RU
 ELSIF tmp = '30EB'  THEN result:= result || '1130EB'; -- KATAKANA LETTER RU
 ELSIF tmp = 'FF99'  THEN result:= result || '12FF99'; -- HALFWIDTH KATAKANA LETTER RU
 ELSIF tmp = '32F8'  THEN result:= result || '1332F8'; -- CIRCLED KATAKANA RU
 ELSIF tmp = '3354'  THEN result:= result || '1C3354'; -- SQUARE RUUBURU
 ELSIF tmp = '3353'  THEN result:= result || '1C3353'; -- SQUARE RUPII
 ELSIF tmp = '308C'  THEN result:= result || '0E308C'; -- HIRAGANA LETTER RE
 ELSIF tmp = '31FE'  THEN result:= result || '0F31FE'; -- KATAKANA LETTER SMALL RE
 ELSIF tmp = '30EC'  THEN result:= result || '1130EC'; -- KATAKANA LETTER RE
 ELSIF tmp = 'FF9A'  THEN result:= result || '12FF9A'; -- HALFWIDTH KATAKANA LETTER RE
 ELSIF tmp = '32F9'  THEN result:= result || '1332F9'; -- CIRCLED KATAKANA RE
 ELSIF tmp = '3355'  THEN result:= result || '1C3355'; -- SQUARE REMU
 ELSIF tmp = '3356'  THEN result:= result || '1C3356'; -- SQUARE RENTOGEN
 ELSIF tmp = '308D'  THEN result:= result || '0E308D'; -- HIRAGANA LETTER RO
 ELSIF tmp = '31FF'  THEN result:= result || '0F31FF'; -- KATAKANA LETTER SMALL RO
 ELSIF tmp = '30ED'  THEN result:= result || '1130ED'; -- KATAKANA LETTER RO
 ELSIF tmp = 'FF9B'  THEN result:= result || '12FF9B'; -- HALFWIDTH KATAKANA LETTER RO
 ELSIF tmp = '32FA'  THEN result:= result || '1332FA'; -- CIRCLED KATAKANA RO
 ELSIF tmp = '308E'  THEN result:= result || '0D308E'; -- HIRAGANA LETTER SMALL WA
 ELSIF tmp = '308F'  THEN result:= result || '0E308F'; -- HIRAGANA LETTER WA
 ELSIF tmp = '30EE'  THEN result:= result || '0F30EE'; -- KATAKANA LETTER SMALL WA
 ELSIF tmp = '30EF'  THEN result:= result || '1130EF'; -- KATAKANA LETTER WA
 ELSIF tmp = 'FF9C'  THEN result:= result || '12FF9C'; -- HALFWIDTH KATAKANA LETTER WA
 ELSIF tmp = '32FB'  THEN result:= result || '1332FB'; -- CIRCLED KATAKANA WA
 ELSIF tmp = '30F7'  THEN result:= result || '1130EF'; -- KATAKANA LETTER VA
 ELSIF tmp = '3357'  THEN result:= result || '1C3357'; -- SQUARE WATTO
 ELSIF tmp = '3090'  THEN result:= result || '0E3090'; -- HIRAGANA LETTER WI
 ELSIF tmp = '30F0'  THEN result:= result || '1130F0'; -- KATAKANA LETTER WI
 ELSIF tmp = '32FC'  THEN result:= result || '1332FC'; -- CIRCLED KATAKANA WI
 ELSIF tmp = '30F8'  THEN result:= result || '1130F0'; -- KATAKANA LETTER VI
 ELSIF tmp = '3091'  THEN result:= result || '0E3091'; -- HIRAGANA LETTER WE
 ELSIF tmp = '30F1'  THEN result:= result || '1130F1'; -- KATAKANA LETTER WE
 ELSIF tmp = '32FD'  THEN result:= result || '1332FD'; -- CIRCLED KATAKANA WE
 ELSIF tmp = '30F9'  THEN result:= result || '1130F1'; -- KATAKANA LETTER VE
 ELSIF tmp = '3092'  THEN result:= result || '0E3092'; -- HIRAGANA LETTER WO
 ELSIF tmp = '30F2'  THEN result:= result || '1130F2'; -- KATAKANA LETTER WO
 ELSIF tmp = 'FF66'  THEN result:= result || '12FF66'; -- HALFWIDTH KATAKANA LETTER WO
 ELSIF tmp = '32FE'  THEN result:= result || '1332FE'; -- CIRCLED KATAKANA WO
 ELSIF tmp = '30FA'  THEN result:= result || '1130F2'; -- KATAKANA LETTER VO
 ELSIF tmp = '3093'  THEN result:= result || '0E3093'; -- HIRAGANA LETTER N
 ELSIF tmp = '30F3'  THEN result:= result || '1130F3'; -- KATAKANA LETTER N
 ELSIF tmp = 'FF9D'  THEN result:= result || '12FF9D'; -- HALFWIDTH KATAKANA LETTER N
