<?php
//$conn_string = "host=localhost port=5432 dbname=papillon user=papillon password=dbpap2";
$conn_string = "dbname=papillon user=papillon password=dbpap2";
$dbconnexion = pg_pconnect($conn_string);

$volume = $_REQUEST['VOLUME'];
$limit = !empty($_REQUEST['LIMIT']) ? $_REQUEST['LIMIT'] : '100';
$direction = !empty($_REQUEST['DIRECTION']) ? $_REQUEST['DIRECTION'] : 'asc';
$headword = !empty($_REQUEST['HEADWORD']) ? 'msort > multilingual_sort(\'est\',\'' . $_REQUEST['HEADWORD'] . '\')' : 'true';
$operator = $direction=='asc' ? '>' : '<';
$msort = !empty($_REQUEST['MSORT']) ? 'msort ' . $operator . ' \'' . $_REQUEST['MSORT'] . '\'' : 'true';
//echo '<!DOCTYPE html><html><head><meta charset="UTF-8" /></head><body>';
echo '<?xml version="1.0"?><div class="entries">';
$sql = 'SELECT value, msort, entryid FROM ' . $volume . ' WHERE ' . $headword . ' AND ' . $msort . ' AND key=\'cdm-headword\' ORDER BY msort '. $direction.' LIMIT ' . $limit;
//echo $sql;
$request=pg_query($sql);
if ($direction=='asc') {
	while($entry=pg_fetch_object($request)) {
		echo '<div class="entry" msort="',$entry->msort,'"><a href="javascript:void(0);" onclick="lookupVolume(\'VOLUME=GDEF_est&HANDLE=', $entry->entryid, '\');$(this).parent().css({ \'font-weight\': \'bold\' })">', $entry->value, '</a></div>';
	}
}
else {
	$result = '';
	while($entry=pg_fetch_object($request)) {
		$result = '<div class="entry" msort="'.$entry->msort.'"><a href="javascript:void(0);" onclick="lookupVolume(\'VOLUME=GDEF_est&HANDLE='.$entry->entryid. '\');$(this).parent().css({ \'font-weight\': \'bold\' })">'. $entry->value. '</a></div>' . $result;
	}
	echo $result;
}
echo '</div>';
?>