<!--
	var volume;
	var limit;
	var load = false; // aucun chargement d'entrée n'est en cours
	var entriesHeight = 0;
 	var nbentries = 0;
 	var lastEntryOffset = 0;
 	var entriesSize = 0;
 	var listTop = false;
 	var listBottom = false;

$(document).ready(function(){ // Quand le document est complètement chargé
	//on cache le loader
	$('.loadmore').hide();
	$('.loadcontent').hide();
	volume = $('#VOLUME').val();
	limit = parseInt($('#LIMIT').val(),10);
  	entriesHeight = $('#entries').height();
 	lastEntryOffset = entriesHeight;
 	//echo 'volume: ',volume;
 
	$('#entries').scroll(function(){ // On surveille l'évènement scroll
 
		/* Si l'élément offset est en bas de scroll, si aucun chargement 
		n'est en cours, et si toutes les entrées ne sont pas affichées, alors on 
		lance la fonction. */
		var scrollTop = $('#entries').scrollTop();
		if ((scrollTop >= lastEntryOffset || scrollTop < 100) && load==false) {
 
 			var direction = (scrollTop >= lastEntryOffset) ? 'asc': 'desc';
 			if ((direction=='asc' && !listBottom) || (direction=='desc' && !listTop)) {

			// la valeur passe à vrai, on va charger
			load = true;
			//On affiche un loader
			$('.loadmore').show();

			//console.log('load: lastEntryOffset: ' + lastEntryOffset + ' scrollTop: ' +scrollTop + ' listTop: ' +listTop);

			//On récupère le headword du dernier entrée affiché
			var msort = (direction=='asc') ? $('.entry:last').attr('msort') : $('.entry:first').attr('msort');
 			var lastEntriesSize = $('.entry').size();
 
			//On lance la fonction ajax
			$.ajax({
				url: './headwords.php',
				type: 'get',
				data: 'VOLUME='+volume+'&LIMIT='+limit+'&MSORT='+msort+'&DIRECTION='+direction,
 
				//Succès de la requête
				success: function(data) {
					//On masque le loader
					$('.loadmore').fadeOut(500);
 					/* On affiche le résultat */
					if (direction=='asc') {
						$('.entry:last').after($(data).children());
						listBottom = ($('.entry').size()-lastEntriesSize<limit);
					}
					else {
						$('.entry:first').before($(data).children());
						listTop = ($('.entry').size()-lastEntriesSize<limit);
 						$('#entries').scrollTop(entriesHeight/2);
					}
					/* On actualise la valeur offset de la dernière entrée */
 					lastEntryOffset = $('.entry:last').offset().top - $('.entry:first').offset().top - entriesHeight;
					//On remet la valeur à faux car c'est fini
					load = false;
				}
			});
			}
		}
	});
});

function query_headword(headword) {
			//On lance la fonction ajax
			load = true;
			$('.loadmore').show();
			$('#entries').children().remove();
			$.ajax({
				url: './headwords.php',
				type: 'get',
				data: 'VOLUME='+volume+'&LIMIT='+limit+'&HEADWORD='+headword+'&DIRECTION=asc',
 
				//Succès de la requête
				success: function(data) {
 
					//On masque le loader
					$('.loadmore').fadeOut(500);
					/* On affiche le résultat après
					le dernier commentaire */
					$('#entries').append($(data).children());
					/* On actualise la valeur offset de la dernière entrée */
 					lastEntryOffset = $('.entry:last').offset().top - $('.entry:first').offset().top - entriesHeight;
					//On remet la valeur à faux car c'est fini
 					$('#entries').scrollTop(entriesHeight/2);
					load = false;
				}
			});
	}
			
function lookupVolume (parameters) {
			//On lance la fonction ajax
			load = true;
			$('.loadcontent').show();
			$('#content').children().remove();
			$('.entry').css('font-weight', 'normal');

			$.ajax({
				url: 'LookupVolume.po',
				type: 'get',
				data: parameters,
 
				//Succès de la requête
				success: function(data) {
 
					//On masque le loader
					$('.loadcontent').fadeOut(500);
					// On affiche le résultat
					//$('#content').text('http://localhost:8999/LookupVolume.po' + parameters);
					$('#content').append(data);
					load = false;
				}
			});
	}			
// -->