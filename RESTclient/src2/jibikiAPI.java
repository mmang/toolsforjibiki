//
//  jibikiAPI.java
//  RESTclient
//
//  Created by Mathieu MANGEOT on 06/10/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import com.sun.jersey.core.util.MultivaluedMapImpl;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;


public class jibikiAPI {
	
	protected Client client = null;
    
    protected static java.nio.charset.CharsetEncoder asciiEncoder =
    java.nio.charset.Charset.forName("US-ASCII").newEncoder(); // or "ISO-8859-1" for ISO Latin 1

    public final static String XML_FORMAT = "XML";
    public final static String JSON_FORMAT = "JSON";
    
	public jibikiAPI () {
		client = Client.create();
	}
	
    
	public RESTanswer get(String resource, String format, String login, String password) {
        javax.ws.rs.core.MediaType mediaType = javax.ws.rs.core.MediaType.APPLICATION_XML_TYPE;
		System.out.println("resource: " + resource);
        WebResource webResource = client.resource(encodeURL(resource));
        System.out.println(javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE);
        //com.sun.jersey.api.client.WebResource.Builder builder = webResource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE);
        
        //webResource.accept(javax.ws.rs.core.MediaType.APPLICATION_XML_TYPE);
//        webResource.options(ClientResponse.class);
        if (login!=null && !login.equals("") && password!=null && !password.equals("")) {
            client.addFilter(new HTTPBasicAuthFilter(login, password));
            System.out.println("l:" + login + " p :" + password);
        }
        if (format.equals(JSON_FORMAT)) {
            mediaType = javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
        }
        ClientResponse response = (ClientResponse) webResource.accept(mediaType).type(MediaType.APPLICATION_XML).get(ClientResponse.class);
        //ClientResponse response = (ClientResponse) webResource.get(ClientResponse.class);
        
		String textEntity = (String) response.getEntity(String.class);
		int status = response.getStatus();
		
		System.out.println("status: " + status);
		
		return new RESTanswer(textEntity,status);
	}
	
	public RESTanswer put(String resource, String format, String entry, String login, String password) {
		System.out.println("resource: " + resource);
		System.out.println("entry: " + entry);
        WebResource webResource = client.resource(encodeURL(resource));
		MultivaluedMap queryParams = new MultivaluedMapImpl();
        if (login!=null && !login.equals("")&& password!=null && !password.equals("")) {
            client.addFilter(new HTTPBasicAuthFilter(login, password));
            System.out.println("l:" + login + " p :" + password);
        }
		
		ClientResponse response = (ClientResponse) webResource.queryParams(queryParams).put(ClientResponse.class, entry);
		String textEntity = (String) response.getEntity(String.class);
		int status = response.getStatus();
		
		System.out.println("status: " + status);
		return new RESTanswer(textEntity,status);
	}
	
	
	public RESTanswer post(String resource, String format, String entry, String login, String password) {
		System.out.println("resource: " + resource);
		System.out.println("entry: " + entry);		
		WebResource webResource = client.resource(encodeURL(resource));
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		if (login!=null && !login.equals("")&& password!=null && !password.equals("")) {
			client.addFilter(new HTTPBasicAuthFilter(login, password));
            String s = webResource.toString();
            System.out.println("l:[" + login + "] p :[" + password + "] qp: " + s);
		}
		ClientResponse response = (ClientResponse) webResource.queryParams(queryParams).post(ClientResponse.class, entry);
		String textEntity = (String) response.getEntity(String.class);
		int status = response.getStatus();
		
		System.out.println("status: " + status);
		return new RESTanswer(textEntity,status);
	}
	
	public RESTanswer delete(String resource, String login, String password) {
		System.out.println("resource: " + resource);
		WebResource webResource = client.resource(encodeURL(resource));

		MultivaluedMap queryParams = new MultivaluedMapImpl();
        if (login!=null && !login.equals("")&& password!=null && !password.equals("")) {
            client.addFilter(new HTTPBasicAuthFilter(login, password));
            System.out.println("l:" + login + " p :" + password);
        }
		
		ClientResponse response = (ClientResponse) webResource.queryParams(queryParams).delete(ClientResponse.class);
		int status = response.getStatus();
        String textEntity = "";
        if (status != 204) {
            textEntity = (String) response.getEntity(String.class);
        }
		
		System.out.println("status: " + status);
		return new RESTanswer(textEntity,status);
	}
    
    protected String encodeURL(String address) {
        String answer = "";
        char[] cArray = address.toCharArray();
        
        for (int i=0; i<cArray.length;i++) {
            char c = cArray[i];
            if (asciiEncoder.canEncode(c)) {
                answer += c;
            }
            else {
                try {
                    answer += java.net.URLEncoder.encode(""+c, "UTF-8");
                }
                catch (Exception e) {
                    answer += c;
                    System.out.println("Error: malformed URL encoding: " + address);
                }
            }
        }
        return answer;
    }
	
	
}
