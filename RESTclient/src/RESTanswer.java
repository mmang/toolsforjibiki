//
//  RESTanswer.java
//  RESTclient
//
//  Created by Mathieu MANGEOT on 18/10/09.
//  Copyright 2009 LISTIC. All rights reserved.
//

public class RESTanswer {
	
	protected int theStatus = 404;
	protected String theObject = "";
	
	public RESTanswer (String object, int status) {
		theStatus = status;
		theObject = object;
	}
	
	public String getObject() {
		return theObject;
	}

	public int getStatus() {
		return theStatus;
	}
	
}
