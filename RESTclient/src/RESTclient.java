//
//  RESTclient.java
//  RESTclient
//
//  Created by Mathieu MANGEOT on 05/10/09.
//  Copyright (c) 2009 __MyCompanyName__. All rights reserved.
//
//	For information on setting Java configuration information, including 
//	setting Java properties, refer to the documentation at
//		http://developer.apple.com/techpubs/java/java.html
//

import java.util.Locale;
import java.util.ResourceBundle;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.BadLocationException;
import javax.swing.JTextArea;

import com.apple.eawt.*;

// test à virer...
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class RESTclient extends JFrame {

	private Font titleFont = new Font("serif", Font.ITALIC+Font.BOLD, 36);
	private Font resultFont = new Font("sans-serif", Font.PLAIN, 12);
	protected ResourceBundle resbundle;
	protected AboutBox aboutBox;
	protected PrefPane prefs;
	private Application fApplication = Application.getApplication();
	protected Action newAction, openAction, closeAction, saveAction, saveAsAction,
					 undoAction, cutAction, copyAction, pasteAction, clearAction, selectAllAction;
	static final JMenuBar mainMenuBar = new JMenuBar();	
	protected JMenu fileMenu, editMenu; 
	
	protected JTextField urlTextField = null;
	protected JTextField loginTextField = null;
	protected JPasswordField passwordTextField = null;
	//protected JTextField statusTextField = null;
	protected JTextArea statusTextField = null;
	protected jibikiAPI myJibikiAPI = new jibikiAPI();
	protected JTextArea resultTextArea = null;
    
    protected String format = "XML";

	
	public RESTclient() {
		
		super("");
		// The ResourceBundle below contains all of the strings used in this
		// application.  ResourceBundles are useful for localizing applications.
		// New localities can be added by adding additional properties files.
		resbundle = ResourceBundle.getBundle ("strings", Locale.getDefault());
		setTitle(resbundle.getString("frameConstructor"));
		Container ContentPane = this.getContentPane();
		SpringLayout layout = new SpringLayout();
		this.getContentPane().setLayout(layout);
		
		createActions();
		addMenus();

		JPanel urlPanel = new JPanel(new BorderLayout());
		JLabel urlLabel = new JLabel("URL: ");
		urlLabel.setDisplayedMnemonic(KeyEvent.VK_U);
		urlTextField = new JTextField(40);
		urlLabel.setLabelFor(urlTextField);
		urlPanel.add(urlLabel);
		urlPanel.add(urlTextField);

		this.getContentPane().add(urlPanel);
		this.getContentPane().add(urlLabel);
		
		layout.putConstraint(SpringLayout.WEST, urlLabel, 40, SpringLayout.WEST, ContentPane);
		layout.putConstraint(SpringLayout.NORTH, urlLabel, 100, SpringLayout.NORTH, ContentPane);
		layout.putConstraint(SpringLayout.WEST, urlPanel, 3, SpringLayout.EAST, urlLabel);
		layout.putConstraint(SpringLayout.NORTH, urlPanel, -7, SpringLayout.NORTH, urlLabel);
		//layout.putConstraint(SpringLayout.WEST, right, 20, SpringLayout.EAST, left);
	
		JPanel loginPanel = new JPanel(new BorderLayout());
		JLabel loginLabel = new JLabel("Login: ");
		loginLabel.setDisplayedMnemonic(KeyEvent.VK_L);
		loginTextField = new JTextField(7);
		loginLabel.setLabelFor(loginTextField);
		loginPanel.add(loginLabel);
		loginPanel.add(loginTextField);
		
		this.getContentPane().add(loginPanel);
		this.getContentPane().add(loginLabel);
		
		layout.putConstraint(SpringLayout.WEST, loginLabel, 0, SpringLayout.WEST, urlLabel);
		layout.putConstraint(SpringLayout.NORTH, loginLabel, 5, SpringLayout.SOUTH, urlPanel);
		layout.putConstraint(SpringLayout.WEST, loginPanel, 3, SpringLayout.EAST, loginLabel);
		layout.putConstraint(SpringLayout.NORTH, loginPanel, -7, SpringLayout.NORTH, loginLabel);
		
		JPanel passwordPanel = new JPanel(new BorderLayout());
		JLabel passwordLabel = new JLabel("Password: ");
		passwordLabel.setDisplayedMnemonic(KeyEvent.VK_P);
		passwordTextField = new JPasswordField(7);
		passwordLabel.setLabelFor(loginTextField);
		passwordPanel.add(passwordLabel);
		passwordPanel.add(passwordTextField);
		
		this.getContentPane().add(passwordPanel);
		this.getContentPane().add(passwordLabel);
		
		layout.putConstraint(SpringLayout.WEST, passwordLabel, 5, SpringLayout.EAST, loginPanel);
		layout.putConstraint(SpringLayout.NORTH, passwordLabel, 0, SpringLayout.NORTH, loginLabel);
		layout.putConstraint(SpringLayout.WEST, passwordPanel, 3, SpringLayout.EAST, passwordLabel);
		layout.putConstraint(SpringLayout.NORTH, passwordPanel, -7, SpringLayout.NORTH, passwordLabel);
		
        
        JLabel popupLabel = new JLabel("Type: ");
        this.getContentPane().add(popupLabel);
        
        layout.putConstraint(SpringLayout.WEST, popupLabel, 5, SpringLayout.EAST, passwordPanel);
        layout.putConstraint(SpringLayout.NORTH, popupLabel, 0, SpringLayout.NORTH, loginLabel);

        JPopupMenu popupMenu = new JPopupMenu();
        
        JMenuItem xmlPopupItem = new JMenuItem(jibikiAPI.XML_FORMAT);
        popupMenu.add(xmlPopupItem);
        xmlPopupItem.setHorizontalTextPosition(JMenuItem.RIGHT);
        JMenuItem jsonPopupItem = new JMenuItem(jibikiAPI.JSON_FORMAT);
        popupMenu.add(jsonPopupItem);
        jsonPopupItem.setHorizontalTextPosition(JMenuItem.RIGHT);

        
        popupMenu.setLabel("Justification");
        popupMenu.setBorder(new BevelBorder(BevelBorder.RAISED));
        popupMenu.setBackground( Color.WHITE );
        
        this.getContentPane().add(popupMenu);
        layout.putConstraint(SpringLayout.WEST, popupMenu, 5, SpringLayout.EAST, popupLabel);
        layout.putConstraint(SpringLayout.NORTH, popupMenu, 0, SpringLayout.NORTH, popupLabel);
       
		
		JButton getButton = new JButton("get");
		getButton.setVerticalTextPosition(AbstractButton.BOTTOM);
		getButton.setHorizontalTextPosition(AbstractButton.CENTER);
		getButton.setActionCommand("get");
		getButton.setDefaultCapable(true);
		
		this.getContentPane().add(getButton);
		layout.putConstraint(SpringLayout.WEST, getButton, 0, SpringLayout.WEST, loginLabel);
		layout.putConstraint(SpringLayout.NORTH, getButton, 8, SpringLayout.SOUTH, loginLabel);
		
		JButton putButton = new JButton("put");
		putButton.setVerticalTextPosition(AbstractButton.BOTTOM);
		putButton.setHorizontalTextPosition(AbstractButton.CENTER);
		putButton.setActionCommand("put");
		this.getContentPane().add(putButton);
		layout.putConstraint(SpringLayout.WEST, putButton, 0, SpringLayout.WEST, getButton);
		layout.putConstraint(SpringLayout.NORTH, putButton, 3, SpringLayout.SOUTH, getButton);
		
		JButton postButton = new JButton("post");
		postButton.setVerticalTextPosition(AbstractButton.BOTTOM);
		postButton.setHorizontalTextPosition(AbstractButton.CENTER);
		postButton.setActionCommand("post");
		this.getContentPane().add(postButton);
		layout.putConstraint(SpringLayout.WEST, postButton, 2, SpringLayout.EAST, getButton);
		layout.putConstraint(SpringLayout.NORTH, postButton, 0, SpringLayout.NORTH, getButton);

		JButton deleteButton = new JButton("delete");
		deleteButton.setVerticalTextPosition(AbstractButton.BOTTOM);
		deleteButton.setHorizontalTextPosition(AbstractButton.CENTER);
		deleteButton.setActionCommand("delete");
		this.getContentPane().add(deleteButton);
		layout.putConstraint(SpringLayout.WEST, deleteButton, 2, SpringLayout.EAST, putButton);
		layout.putConstraint(SpringLayout.NORTH, deleteButton, 0, SpringLayout.NORTH, putButton);
		

		JPanel statusPanel = new JPanel(new BorderLayout());
		JLabel statusLabel = new JLabel("Status: ");
		statusLabel.setDisplayedMnemonic(KeyEvent.VK_O);
		//statusTextField = new JTextField(5);
		statusTextField = new JTextArea(1,3);		

		statusLabel.setLabelFor(statusTextField);
		statusPanel.add(statusLabel);
		statusPanel.add(statusTextField);
		
		this.getContentPane().add(statusPanel);
		this.getContentPane().add(statusLabel);
		
		layout.putConstraint(SpringLayout.WEST, statusLabel, 10, SpringLayout.EAST, postButton);
		layout.putConstraint(SpringLayout.NORTH, statusLabel, 2, SpringLayout.NORTH, statusPanel);
		layout.putConstraint(SpringLayout.WEST, statusPanel, 3, SpringLayout.EAST, statusLabel);
		layout.putConstraint(SpringLayout.NORTH, statusPanel, 0, SpringLayout.NORTH, postButton);
		
		
		
		resultTextArea = new JTextArea(30,40);		
		resultTextArea.setLineWrap(true);
		resultTextArea.setWrapStyleWord(true);
		JScrollPane scrollingArea = new JScrollPane(resultTextArea);
		this.getContentPane().add(scrollingArea);
		layout.putConstraint(SpringLayout.NORTH, scrollingArea, 130, SpringLayout.NORTH, urlPanel);
		layout.putConstraint(SpringLayout.WEST, scrollingArea, 0, SpringLayout.WEST, urlLabel);
		//layout.putConstraint(SpringLayout.EAST, scrollingArea, 40, SpringLayout.EAST, ContentPane);
		//layout.putConstraint(SpringLayout.SOUTH, scrollingArea, 40, SpringLayout.SOUTH, ContentPane);
		

		
		ActionListener actionListener = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
                if (actionEvent.getActionCommand().equals("XML")) {
                    format = "XML";
                    System.out.println("format: XML");
                }
                if (actionEvent.getActionCommand().equals("JSON")) {
                    format = "JSON";
                   System.out.println("format: JSON");
                }
				if (actionEvent.getActionCommand().equals("get")) {
					String url = getUrl();
                    String login = getLogin();
                    String password = getPassword();
					System.out.println(actionEvent.getActionCommand() + ":" + url);
					RESTanswer theAnswer = myJibikiAPI.get(url,format, login, password);
					resultTextArea.setText(theAnswer.getObject());
					statusTextField.setText(""+theAnswer.getStatus());
				}
				if (actionEvent.getActionCommand().equals("put")) {
					String url = getUrl(); 
					String login = getLogin(); 
					String password = getPassword(); 
					System.out.println(actionEvent.getActionCommand() + ":" + url);
					RESTanswer theAnswer = myJibikiAPI.put(url, format, resultTextArea.getText(), login, password);
					resultTextArea.setText(theAnswer.getObject());
					statusTextField.setText(""+theAnswer.getStatus());
				}
				if (actionEvent.getActionCommand().equals("post")) {
					String url = getUrl(); 
					String login = getLogin(); 
					String password = getPassword(); 
					System.out.println(actionEvent.getActionCommand() + ":" + url);
					RESTanswer theAnswer = myJibikiAPI.post(url, format, resultTextArea.getText(), login, password);
					resultTextArea.setText(theAnswer.getObject());
					statusTextField.setText(""+theAnswer.getStatus());
				}
				if (actionEvent.getActionCommand().equals("delete")) {
					String url = getUrl(); 
					String login = getLogin(); 
					String password = getPassword(); 
					System.out.println(actionEvent.getActionCommand() + ":" + url);
					RESTanswer theAnswer = myJibikiAPI.delete(url, login, password);
					resultTextArea.setText(theAnswer.getObject());
					statusTextField.setText(""+theAnswer.getStatus());
				}
			}
		};
		urlTextField.setActionCommand("get");
		urlTextField.addActionListener(actionListener);
		getButton.addActionListener(actionListener);
		putButton.addActionListener(actionListener);
		postButton.addActionListener(actionListener);
		deleteButton.addActionListener(actionListener);
        xmlPopupItem.addActionListener(actionListener);
        jsonPopupItem.addActionListener(actionListener);
	
		
		KeyListener keyListener = new KeyListener() {
			public void keyPressed(KeyEvent keyEvent) {
				printIt("Pressed", keyEvent);
			}
			
			public void keyReleased(KeyEvent keyEvent) {
				printIt("Released", keyEvent);
			}
			
			public void keyTyped(KeyEvent keyEvent) {
				printIt("Typed", keyEvent);
			}
			
			private void printIt(String title, KeyEvent keyEvent) {
				int keyCode = keyEvent.getKeyCode();
				String keyText = KeyEvent.getKeyText(keyCode);
				System.out.println(title + " : " + keyText + " / "
								   + keyEvent.getKeyChar());
			}
		};
		//nameTextField.addKeyListener(keyListener);
		
		DocumentListener documentListener = new DocumentListener() {
			public void changedUpdate(DocumentEvent documentEvent) {
				printIt(documentEvent);
			}
			
			public void insertUpdate(DocumentEvent documentEvent) {
				printIt(documentEvent);
			}
			
			public void removeUpdate(DocumentEvent documentEvent) {
				printIt(documentEvent);
			}
			
			private void printIt(DocumentEvent documentEvent) {
				DocumentEvent.EventType type = documentEvent.getType();
				String typeString = null;
				if (type.equals(DocumentEvent.EventType.CHANGE)) {
					typeString = "Change";
				} else if (type.equals(DocumentEvent.EventType.INSERT)) {
					typeString = "Insert";
				} else if (type.equals(DocumentEvent.EventType.REMOVE)) {
					typeString = "Remove";
				}
				System.out.print("Type  :   " + typeString + " / ");
				Document source = documentEvent.getDocument();
				int length = source.getLength();
				try {
					System.out.println("Contents: " + source.getText(0, length));
				} catch (BadLocationException badLocationException) {
					System.out.println("Contents: Unknown");
				}
			}
		};
		urlTextField.getDocument().addDocumentListener(documentListener);
		
		
		fApplication.setEnabledPreferencesMenu(true);
		fApplication.addApplicationListener(new com.apple.eawt.ApplicationAdapter() {
			public void handleAbout(ApplicationEvent e) {
                                if (aboutBox == null) {
                                    aboutBox = new AboutBox();
                                }
                                about(e);
                                e.setHandled(true);
			}
			public void handleOpenApplication(ApplicationEvent e) {
			}
			public void handleOpenFile(ApplicationEvent e) {
			}
			public void handlePreferences(ApplicationEvent e) {
                                if (prefs == null) {
                                    prefs = new PrefPane();
                                }
				preferences(e);
			}
			public void handlePrintFile(ApplicationEvent e) {
			}
			public void handleQuit(ApplicationEvent e) {
				quit(e);
			}
		});
		
		setSize(600, 800);
		setVisible(true);
	}

	public void about(ApplicationEvent e) {
		aboutBox.setResizable(false);
		aboutBox.setVisible(true);
	}

	public void preferences(ApplicationEvent e) {
		prefs.setResizable(false);
		prefs.setVisible(true);
	}

	public void quit(ApplicationEvent e) {	
		System.exit(0);
	}

	public void createActions() {
		int shortcutKeyMask = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

		//Create actions that can be used by menus, buttons, toolbars, etc.
		newAction = new newActionClass( resbundle.getString("newItem"),
											KeyStroke.getKeyStroke(KeyEvent.VK_N, shortcutKeyMask) );
		openAction = new openActionClass( resbundle.getString("openItem"),
											KeyStroke.getKeyStroke(KeyEvent.VK_O, shortcutKeyMask) );
		closeAction = new closeActionClass( resbundle.getString("closeItem"),
											KeyStroke.getKeyStroke(KeyEvent.VK_W, shortcutKeyMask) );
		saveAction = new saveActionClass( resbundle.getString("saveItem"),
											KeyStroke.getKeyStroke(KeyEvent.VK_S, shortcutKeyMask) );
		saveAsAction = new saveAsActionClass( resbundle.getString("saveAsItem") );

		undoAction = new undoActionClass( resbundle.getString("undoItem"),
											KeyStroke.getKeyStroke(KeyEvent.VK_Z, shortcutKeyMask) );
		cutAction = new cutActionClass( resbundle.getString("cutItem"),
											KeyStroke.getKeyStroke(KeyEvent.VK_X, shortcutKeyMask) );
		copyAction = new copyActionClass( resbundle.getString("copyItem"),
											KeyStroke.getKeyStroke(KeyEvent.VK_C, shortcutKeyMask) );
		pasteAction = new pasteActionClass( resbundle.getString("pasteItem"),
											KeyStroke.getKeyStroke(KeyEvent.VK_V, shortcutKeyMask) );
		clearAction = new clearActionClass( resbundle.getString("clearItem") );
		selectAllAction = new selectAllActionClass( resbundle.getString("selectAllItem"),
											KeyStroke.getKeyStroke(KeyEvent.VK_A, shortcutKeyMask) );
	}
	
	public void addMenus() {

		fileMenu = new JMenu(resbundle.getString("fileMenu"));
		fileMenu.add(new JMenuItem(newAction));
		fileMenu.add(new JMenuItem(openAction));
		fileMenu.add(new JMenuItem(closeAction));
		fileMenu.add(new JMenuItem(saveAction));
		fileMenu.add(new JMenuItem(saveAsAction));
		mainMenuBar.add(fileMenu);

		editMenu = new JMenu(resbundle.getString("editMenu"));
		editMenu.add(new JMenuItem(undoAction));
		editMenu.addSeparator();
		editMenu.add(new JMenuItem(cutAction));
		editMenu.add(new JMenuItem(copyAction));
		editMenu.add(new JMenuItem(pasteAction));
		editMenu.add(new JMenuItem(clearAction));
		editMenu.addSeparator();
		editMenu.add(new JMenuItem(selectAllAction));
		mainMenuBar.add(editMenu);

		setJMenuBar (mainMenuBar);
	}

	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.blue);
		g.setFont (titleFont);
		g.drawString(resbundle.getString("message"), 40, 70);
	}
	
	public class newActionClass extends AbstractAction {
		public newActionClass(String text, KeyStroke shortcut) {
			super(text);
			putValue(ACCELERATOR_KEY, shortcut);
		}
		public void actionPerformed(ActionEvent e) {
			System.out.println("New...");
		}
	}

	public class openActionClass extends AbstractAction {
		public openActionClass(String text, KeyStroke shortcut) {
			super(text);
			putValue(ACCELERATOR_KEY, shortcut);
		}
		public void actionPerformed(ActionEvent e) {
			System.out.println("Open...");
		}
	}
	
	public class closeActionClass extends AbstractAction {
		public closeActionClass(String text, KeyStroke shortcut) {
			super(text);
			putValue(ACCELERATOR_KEY, shortcut);
		}
		public void actionPerformed(ActionEvent e) {
			System.out.println("Close...");
		}
	}
	
	public class saveActionClass extends AbstractAction {
		public saveActionClass(String text, KeyStroke shortcut) {
			super(text);
			putValue(ACCELERATOR_KEY, shortcut);
		}
		public void actionPerformed(ActionEvent e) {
			System.out.println("Save...");
		}
	}
	
	public class saveAsActionClass extends AbstractAction {
		public saveAsActionClass(String text) {
			super(text);
		}
		public void actionPerformed(ActionEvent e) {
			System.out.println("Save As...");
		}
	}
	
	public class undoActionClass extends AbstractAction {
		public undoActionClass(String text, KeyStroke shortcut) {
			super(text);
			putValue(ACCELERATOR_KEY, shortcut);
		}
		public void actionPerformed(ActionEvent e) {
			System.out.println("Undo...");
		}
	}
	
	public class cutActionClass extends AbstractAction {
		public cutActionClass(String text, KeyStroke shortcut) {
			super(text);
			putValue(ACCELERATOR_KEY, shortcut);
		}
		public void actionPerformed(ActionEvent e) {
			System.out.println("Cut...");
		}
	}
	
	public class copyActionClass extends AbstractAction {
		public copyActionClass(String text, KeyStroke shortcut) {
			super(text);
			putValue(ACCELERATOR_KEY, shortcut);
		}
		public void actionPerformed(ActionEvent e) {
			System.out.println("Copy...");
		}
	}
	
	public class pasteActionClass extends AbstractAction {
		public pasteActionClass(String text, KeyStroke shortcut) {
			super(text);
			putValue(ACCELERATOR_KEY, shortcut);
		}
		public void actionPerformed(ActionEvent e) {
			System.out.println("Paste...");
		}
	}
	
	public class clearActionClass extends AbstractAction {
		public clearActionClass(String text) {
			super(text);
		}
		public void actionPerformed(ActionEvent e) {
			System.out.println("Clear...");
		}
	}
	
	public class selectAllActionClass extends AbstractAction {
		public selectAllActionClass(String text, KeyStroke shortcut) {
			super(text);
			putValue(ACCELERATOR_KEY, shortcut);
		}
		public void actionPerformed(ActionEvent e) {
			System.out.println("Select All...");
		}
	}
	
	
	public String getUrl() {
		String url = "";
		try {
			url = urlTextField.getDocument().getText(0,urlTextField.getDocument().getLength());
		}
		catch (BadLocationException badLocationException) {
			System.out.println("Contents: Unknown");
		}
		return url;
	}
	
	public String getLogin() {
		String login = "";
		try {
			login = loginTextField.getDocument().getText(0,loginTextField.getDocument().getLength());
		}
		catch (BadLocationException badLocationException) {
			System.out.println("Contents: Unknown");
		}
		return login;
	}
	
	public String getPassword() {
		String password = "";
		try {
			password = passwordTextField.getDocument().getText(0,passwordTextField.getDocument().getLength());
		}
		catch (BadLocationException badLocationException) {
			System.out.println("Contents: Unknown");
		}
		return password;
	}
	
	
	 public static void main(String args[]) {
		new RESTclient();
	 }
}